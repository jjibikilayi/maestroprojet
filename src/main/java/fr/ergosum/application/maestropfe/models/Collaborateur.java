package fr.ergosum.application.maestropfe.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name="collaborateur")
public class Collaborateur extends User {

    private String phone;
    private String adresse;
    @OneToMany(mappedBy = "collaborateurId" , cascade = CascadeType.ALL)
    private List<Tache> listetaches;

    public Collaborateur() {
    }


    public Collaborateur(String phone, String adresse, List<Tache> listetaches) {
        this.phone = phone;
        this.adresse = adresse;
        this.listetaches = listetaches;
    }

    @JsonIgnore
    public List<Tache> getListetaches() {
        return listetaches;
    }

    public void setListetaches(List<Tache> listetaches) {
        this.listetaches = listetaches;
    }

    public Collaborateur(String username, String email, String password, String phone, String adresse, List<Tache> listetaches) {
        super(username, email, password);
        this.phone = phone;
        this.adresse = adresse;
        this.listetaches = listetaches;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
}
