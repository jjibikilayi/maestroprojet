package fr.ergosum.application.maestropfe.models;

public record Message(String name) {
}
