package fr.ergosum.application.maestropfe.models;

public record Greeting(String content) {
}
