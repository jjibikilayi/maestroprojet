package fr.ergosum.application.maestropfe.models;

public enum ERole {
  ROLE_Collaborateur,
  ROLE_Consultant,
  ROLE_Chef
}
