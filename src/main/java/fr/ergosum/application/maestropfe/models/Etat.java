package fr.ergosum.application.maestropfe.models;

public enum Etat {
    INPROCESS,DONE,FINISHED,CANCELED
}
