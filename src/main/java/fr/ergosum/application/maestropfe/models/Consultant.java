package fr.ergosum.application.maestropfe.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;

@Entity
@Table(name="consultant")
public class Consultant extends User {

    public Consultant(String username, String email, String password, List<Projet> projetList) {
        super(username, email, password);
        this.projetList = projetList;
    }

    public Consultant() {
    }

    @OneToMany(mappedBy = "consultantID", cascade = CascadeType.ALL)
    private List<Projet> projetList;


    @JsonIgnore
    public List<Projet> getProjetList() {
        return projetList;
    }

    public void setProjetList(List<Projet> projetList) {
        this.projetList = projetList;
    }

    public Consultant(List<Projet> projetList) {
        this.projetList = projetList;
    }


}
