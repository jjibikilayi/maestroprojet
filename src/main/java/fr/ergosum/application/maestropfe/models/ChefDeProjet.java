package fr.ergosum.application.maestropfe.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.List;


@Entity
@Table(name="chefdeprojet")
public class ChefDeProjet extends User {

    public ChefDeProjet() {
    }

    @OneToMany(mappedBy = "chefProjetID", cascade = CascadeType.ALL)
    private List<Projet> listprojets;

    public ChefDeProjet(String username, String email, String password, List<Projet> listprojets) {
        super(username, email, password);
        this.listprojets = listprojets;
    }

    @JsonIgnore
    public List<Projet> getListprojets() {
        return listprojets;
    }

    public void setListprojets(List<Projet> listprojets) {
        this.listprojets = listprojets;
    }



}
