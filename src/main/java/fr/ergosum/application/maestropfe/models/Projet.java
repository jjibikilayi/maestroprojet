package fr.ergosum.application.maestropfe.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name="projet")
public class Projet {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private Date dateDebut;
    private Date dateFin;

    private String description;
    private String file;


    @OneToMany(mappedBy = "projet", cascade = CascadeType.ALL)
    private List<Tache>  listDetaches;


    //relation entre projet et chef de projet
    @ManyToOne
    @JoinColumn(name="chef_projet")
    private ChefDeProjet chefProjetID;

    //relation entre consultannt et projet
    @ManyToOne
    @JoinColumn(name="consultant")
    private Consultant consultantID;

    public Projet() {
    }

    public Projet(Long id, String nom, Date dateDebut, Date dateFin, String description, String file, List<Tache> listDetaches, ChefDeProjet chefProjetID, Consultant consultantID) {
        this.id = id;
        this.nom = nom;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.description = description;
        this.file = file;
        this.listDetaches = listDetaches;
        this.chefProjetID = chefProjetID;
        this.consultantID = consultantID;
    }


    public void setConsultantID(Consultant consultantID) {
        this.consultantID = consultantID;
    }

    public Consultant getConsultantID() {
        return consultantID;
    }

    public ChefDeProjet getChefProjetID() {
        return chefProjetID;
    }

    public void setChefProjetID(ChefDeProjet chefProjetID) {
        this.chefProjetID = chefProjetID;
    }

    @JsonIgnore
    public List<Tache> getListDetaches() {
        return listDetaches;
    }

    public void setListDetaches(List<Tache> listDetaches) {
        this.listDetaches = listDetaches;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
