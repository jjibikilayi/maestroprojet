package fr.ergosum.application.maestropfe.models;


import jakarta.persistence.*;

import java.util.Date;

@Entity
@Table(name="tache")
public class Tache {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    private String description;
    private Date dateDebut;
    private Date dateFin;
    private String file;
    private Etat etat;

    @ManyToOne
    @JoinColumn(name="projet_Id")
    private Projet projet;


    @ManyToOne
     @JoinColumn(name="collaborateurID")
    private Collaborateur collaborateurId;







    public Tache() {
    }


    public Tache(Long id, String nom, String description, Date dateDebut, Date dateFin, String file, Etat etat, Projet projet, Collaborateur collaborateurId, ChefDeProjet chefDeProjet) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.file = file;
        this.etat = etat;
        this.projet = projet;
        this.collaborateurId = collaborateurId;

    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Collaborateur getCollaborateurId() {
        return collaborateurId;
    }

    public void setCollaborateurId(Collaborateur collaborateurId) {
        this.collaborateurId = collaborateurId;
    }

    public Projet getProjet() {
        return projet;
    }

    public void setProjet(Projet projet) {
        this.projet = projet;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public Etat getEtat() {
        return etat;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }



}
