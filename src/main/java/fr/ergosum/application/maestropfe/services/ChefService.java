package fr.ergosum.application.maestropfe.services;

import fr.ergosum.application.maestropfe.models.ChefDeProjet;

import java.util.List;

public interface ChefService {


    public ChefDeProjet createChefdeProjet(ChefDeProjet chef);
    public ChefDeProjet modifierChefDeProjet(ChefDeProjet chef);
    public void supprimerChefDeProjet(Long id);

    public List<ChefDeProjet> listChefDeProjet();

    public ChefDeProjet readOneChef(Long id);
}
