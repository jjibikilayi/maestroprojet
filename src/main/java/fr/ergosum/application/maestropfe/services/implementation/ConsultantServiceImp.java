package fr.ergosum.application.maestropfe.services.implementation;


import fr.ergosum.application.maestropfe.models.Consultant;
import fr.ergosum.application.maestropfe.repositories.ConsultantRepository;
import fr.ergosum.application.maestropfe.services.ConsultantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsultantServiceImp implements ConsultantService {


    @Autowired
    ConsultantRepository consultantRepository;



    @Override
    public Consultant createConsultant(Consultant cons) {
        return consultantRepository.save(cons);
    }

    @Override
    public Consultant modifierConsultant(Consultant cons) {
        return consultantRepository.save(cons);
    }

    @Override
    public void supprimerConsultant(Long id) {
        consultantRepository.deleteById(id);
    }

    @Override
    public List<Consultant> listConsultant() {
        return consultantRepository.findAll();
    }

    @Override
    public Consultant readOneConsultant(Long id) {
        return consultantRepository.findById(id).orElse(null);
    }
}
