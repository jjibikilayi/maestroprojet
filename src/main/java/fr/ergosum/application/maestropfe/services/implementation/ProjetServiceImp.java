package fr.ergosum.application.maestropfe.services.implementation;


import fr.ergosum.application.maestropfe.models.Projet;
import fr.ergosum.application.maestropfe.repositories.ProjetRepository;
import fr.ergosum.application.maestropfe.services.ProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjetServiceImp implements ProjetService {

    @Autowired
    ProjetRepository projetRepository;


    @Override
    public Projet createProjet(Projet projet) {
        return projetRepository.save(projet);
    }

    @Override
    public Projet modifierProjet(Projet projet) {
        return projetRepository.save(projet);
    }

    @Override
    public void supprimerProjet(Long id) {

        projetRepository.deleteById(id);

    }

    @Override
    public List<Projet> listProjet() {
        return projetRepository.findAll();
    }

    @Override
    public Projet readOneProjet(Long id) {
        return projetRepository.findById(id).orElse(null);
    }
}
