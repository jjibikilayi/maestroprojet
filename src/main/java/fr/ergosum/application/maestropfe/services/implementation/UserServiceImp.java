package fr.ergosum.application.maestropfe.services.implementation;


import fr.ergosum.application.maestropfe.models.User;
import fr.ergosum.application.maestropfe.repositories.UserRepository;
import fr.ergosum.application.maestropfe.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements UserService {



    @Autowired
    private UserRepository userRepository;




    @Override
    public User createUser(User user) {
       return  userRepository.save(user);
    }

    @Override
    public User updateUser(User user) {
        return  userRepository.save(user);
    }

    @Override
    public List<User> allUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteUser(Long id) {
     userRepository.deleteById(id);
    }
}
