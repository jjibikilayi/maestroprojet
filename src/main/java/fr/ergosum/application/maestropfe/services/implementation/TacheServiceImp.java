package fr.ergosum.application.maestropfe.services.implementation;


import fr.ergosum.application.maestropfe.models.Tache;
import fr.ergosum.application.maestropfe.repositories.TacheRepository;
import fr.ergosum.application.maestropfe.services.TacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TacheServiceImp implements TacheService {

    @Autowired
    TacheRepository tacheRepository;


    @Override
    public Tache createTache(Tache tache) {
        return  tacheRepository.save(tache);
    }

    @Override
    public Tache modifierTache(Tache tache) {
        return tacheRepository.save(tache);
    }

    @Override
    public void supprimerTache(Long id) {

        tacheRepository.deleteById(id);

    }

    @Override
    public List<Tache> listTache() {
        return tacheRepository.findAll();
    }

    @Override
    public Tache readOne(Long id) {
        return tacheRepository.findById(id).orElse(null);
    }
}
