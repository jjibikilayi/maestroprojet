package fr.ergosum.application.maestropfe.services;

import fr.ergosum.application.maestropfe.models.User;

import java.util.List;

public interface UserService {
    User createUser (User user);
    User updateUser (User user);
    List<User> allUsers();
    User findUserById(Long id);
    void deleteUser(Long id);
}
