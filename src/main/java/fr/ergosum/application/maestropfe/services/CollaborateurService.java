package fr.ergosum.application.maestropfe.services;

import fr.ergosum.application.maestropfe.models.Collaborateur;

import java.util.List;

public interface CollaborateurService {

    Collaborateur createCollaborateur(Collaborateur col);
    Collaborateur modifierCollaborateur( Collaborateur col);
    Collaborateur  readOneCollaborateur(Long id);
    List<Collaborateur> listCollaborateur();
    void supprimerCollaborateur(Long id);




}
