package fr.ergosum.application.maestropfe.services;

import fr.ergosum.application.maestropfe.models.Tache;

import java.util.List;

public interface TacheService {


    public Tache createTache(Tache tache);
    public Tache modifierTache(Tache tache);
    public void supprimerTache(Long id);

    public List<Tache> listTache();

    public Tache readOne(Long id);

}
