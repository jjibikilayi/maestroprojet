package fr.ergosum.application.maestropfe.services;
import  fr.ergosum.application.maestropfe.models.Projet;




import java.util.List;

public interface ProjetService {

    public Projet createProjet(Projet projet);
    public Projet modifierProjet(Projet projet);
    public void supprimerProjet(Long id);

    public List<Projet> listProjet();

    public Projet readOneProjet(Long id);

}
