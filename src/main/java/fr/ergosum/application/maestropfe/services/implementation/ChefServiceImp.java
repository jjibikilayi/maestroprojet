package fr.ergosum.application.maestropfe.services.implementation;


import fr.ergosum.application.maestropfe.models.ChefDeProjet;
import fr.ergosum.application.maestropfe.repositories.ChefProjetRepository;
import fr.ergosum.application.maestropfe.services.ChefService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChefServiceImp implements ChefService {


    @Autowired
    ChefProjetRepository chefProjetRepository;





    @Override
    public ChefDeProjet createChefdeProjet(ChefDeProjet chef) {
        return chefProjetRepository.save(chef);
    }



    @Override
    public ChefDeProjet modifierChefDeProjet(ChefDeProjet chef) {
        return chefProjetRepository.save(chef);
    }

    @Override
    public void supprimerChefDeProjet(Long id) {
      chefProjetRepository.deleteById(id);
    }

    @Override
    public List<ChefDeProjet> listChefDeProjet() {
        return chefProjetRepository.findAll();
    }

    @Override
    public ChefDeProjet readOneChef(Long id) {
        return chefProjetRepository.findById(id).orElse(null);
    }
}
