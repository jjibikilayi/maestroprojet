package fr.ergosum.application.maestropfe.services.implementation;

import fr.ergosum.application.maestropfe.models.Admin;
import fr.ergosum.application.maestropfe.repositories.AdminRepository;
import fr.ergosum.application.maestropfe.services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AdminServiceImp implements AdminService {


    @Autowired
    AdminRepository adminRepository;


    @Override
    public Admin createAdmin(Admin ad) {
        return adminRepository.save(ad);
    }

    @Override
    public Admin modifierAdmin(Admin ad, Long id) {
       return adminRepository.save(ad);
    }

    @Override
    public void supprimerAdmin(Long id) {


        adminRepository.deleteById(id);

    }

    @Override
    public List<Admin> listAdmin() {
        return adminRepository.findAll();
    }

    @Override
    public Admin readOne(Long id) {
        return adminRepository.findById(id).orElse(null);
    }
}
