package fr.ergosum.application.maestropfe.services;

import fr.ergosum.application.maestropfe.models.Admin;

import java.util.List;

public interface AdminService {

   public Admin createAdmin(Admin ad);
    public Admin modifierAdmin(Admin ad, Long id);
    public void supprimerAdmin(Long id);
    public List<Admin> listAdmin();
   public Admin readOne(Long id);





}
