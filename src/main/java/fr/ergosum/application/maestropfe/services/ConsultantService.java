package fr.ergosum.application.maestropfe.services;

import fr.ergosum.application.maestropfe.models.Consultant;

import java.util.List;

public interface ConsultantService {


    public Consultant createConsultant(Consultant cons);
    public Consultant modifierConsultant(Consultant cons);
    public void supprimerConsultant(Long id);

    public List<Consultant> listConsultant();

    public Consultant readOneConsultant(Long id);
}
