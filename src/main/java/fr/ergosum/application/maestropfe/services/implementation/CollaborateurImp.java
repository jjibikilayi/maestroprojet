package fr.ergosum.application.maestropfe.services.implementation;


import fr.ergosum.application.maestropfe.models.Collaborateur;
import fr.ergosum.application.maestropfe.repositories.CollaborateurRepository;
import fr.ergosum.application.maestropfe.services.CollaborateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollaborateurImp implements CollaborateurService {



    @Autowired
    CollaborateurRepository collaborateurRepository;

    @Override
    public Collaborateur createCollaborateur(Collaborateur col) {
        return collaborateurRepository.save(col);
    }

    @Override
    public Collaborateur modifierCollaborateur(Collaborateur col) {
        return collaborateurRepository.save(col);
    }

    @Override
    public Collaborateur readOneCollaborateur(Long id) {
        return collaborateurRepository.findById(id).orElse(null);
    }

    @Override
    public List<Collaborateur> listCollaborateur() {
        return collaborateurRepository.findAll();
    }

    @Override
    public void supprimerCollaborateur(Long id) {collaborateurRepository.deleteById(id);}
}
