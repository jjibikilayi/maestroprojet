package fr.ergosum.application.maestropfe.repositories;

import fr.ergosum.application.maestropfe.models.Consultant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ConsultantRepository extends JpaRepository<Consultant,Long> {
}
