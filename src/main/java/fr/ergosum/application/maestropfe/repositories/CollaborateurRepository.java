package fr.ergosum.application.maestropfe.repositories;


import fr.ergosum.application.maestropfe.models.Collaborateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CollaborateurRepository extends JpaRepository<Collaborateur,Long> {
}
