package fr.ergosum.application.maestropfe.repositories;


import fr.ergosum.application.maestropfe.models.ChefDeProjet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChefProjetRepository extends JpaRepository<ChefDeProjet,Long> {
}
