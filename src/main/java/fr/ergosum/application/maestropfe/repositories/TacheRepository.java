package fr.ergosum.application.maestropfe.repositories;


import fr.ergosum.application.maestropfe.models.Tache;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TacheRepository extends JpaRepository <Tache, Long>{
}
