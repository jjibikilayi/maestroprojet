package fr.ergosum.application.maestropfe.repositories;

import fr.ergosum.application.maestropfe.models.Projet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjetRepository extends JpaRepository <Projet,Long> {
}
