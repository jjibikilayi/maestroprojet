package fr.ergosum.application.maestropfe.repositories;

import java.util.Optional;

import fr.ergosum.application.maestropfe.models.ERole;
import fr.ergosum.application.maestropfe.models.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(ERole name);
}
