package fr.ergosum.application.maestropfe.repositories;


import fr.ergosum.application.maestropfe.models.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepository extends JpaRepository<Admin,Long> {




}
