package fr.ergosum.application.maestropfe.repositories;

import fr.ergosum.application.maestropfe.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
    User findFirstByEmail (String email);
    User findByPasswordResetToken(String passwordResetToken);
}
