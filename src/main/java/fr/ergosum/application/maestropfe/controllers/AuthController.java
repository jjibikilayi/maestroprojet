package fr.ergosum.application.maestropfe.controllers;


import fr.ergosum.application.maestropfe.exception.TokenRefreshException;

import fr.ergosum.application.maestropfe.models.ERole;
import fr.ergosum.application.maestropfe.models.RefreshToken;
import fr.ergosum.application.maestropfe.models.Role;
import fr.ergosum.application.maestropfe.models.User;
import fr.ergosum.application.maestropfe.payload.request.LoginRequest;
import fr.ergosum.application.maestropfe.payload.request.SignupRequest;
import fr.ergosum.application.maestropfe.payload.request.TokenRefreshRequest;
import fr.ergosum.application.maestropfe.payload.response.JwtResponse;
import fr.ergosum.application.maestropfe.payload.response.MessageResponse;
import fr.ergosum.application.maestropfe.payload.response.TokenRefreshResponse;
import fr.ergosum.application.maestropfe.repositories.RoleRepository;
import fr.ergosum.application.maestropfe.repositories.UserRepository;
import fr.ergosum.application.maestropfe.security.jwt.JwtUtils;
import fr.ergosum.application.maestropfe.security.services.RefreshTokenService;
import fr.ergosum.application.maestropfe.security.services.UserDetailsImpl;

import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMessage;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    RefreshTokenService refreshTokenService;

    //login
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
//optinal tnajem tkoun null
        Optional<User> u=userRepository.findByUsername(loginRequest.getUsername());

        if (u.get().isConfirm()==true) {
            SecurityContextHolder.getContext().setAuthentication(authentication);

            UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

            String jwt = jwtUtils.generateJwtToken(userDetails);

            List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
                    .collect(Collectors.toList());

            RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getId());

            return ResponseEntity.ok(new JwtResponse(jwt, refreshToken.getToken(), userDetails.getId(),
                    userDetails.getUsername(), userDetails.getEmail(), roles));

        }else {
            throw new RuntimeException("user not confirmed");
        }

    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid SignupRequest signUpRequest) throws MessagingException {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(), signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRole();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_Collaborateur)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "ROLE_Chef":
                        Role adminRole = roleRepository.findByName(ERole.ROLE_Chef)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "ROLE_Consultant":
                        Role modRole = roleRepository.findByName(ERole.ROLE_Consultant)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(modRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_Collaborateur)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);
        //send mail when create a new provider
        String from ="itService.mail.fr";
        String to=user.getEmail();
        String subject ="confirmation";
        MimeMessage message = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper=new MimeMessageHelper(message);
        messageHelper.setSubject(subject);
        messageHelper.setTo(to);
        messageHelper.setFrom(from);
        messageHelper.setText("<HTML><body>" +
                " <a href=\"http://localhost:8085/api/auth/confirm?email="
                +signUpRequest.getEmail()+"\">VERIFY</a></body></HTML>",true);

        javaMailSender.send(message);//envoyer email

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshtoken(@Valid @RequestBody TokenRefreshRequest request) {
        String requestRefreshToken = request.getRefreshToken();

        return refreshTokenService.findByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    String token = jwtUtils.generateTokenFromUsername(user.getUsername());
                    return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
                })
                .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
                        "Refresh token is not in database!"));
    }

    @PostMapping("/signout")
    public ResponseEntity<?> logoutUser() {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Long userId = userDetails.getId();
        refreshTokenService.deleteByUserId(userId);
        return ResponseEntity.ok(new MessageResponse("Log out successful!"));
    }
    @GetMapping("/confirm")
    public ResponseEntity<?> confirm( @RequestParam String email) {


        // Create new user's account
        User user = userRepository.findFirstByEmail(email);
        if(user != null){
            user.setConfirm(true);
            userRepository.save(user);
            return ResponseEntity.ok(new MessageResponse("user is confirmed"));
        }
        return ResponseEntity.ok(new MessageResponse("User not confirmed!"));
    }



}
