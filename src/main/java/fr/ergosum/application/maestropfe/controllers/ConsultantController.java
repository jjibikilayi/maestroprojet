package fr.ergosum.application.maestropfe.controllers;



import fr.ergosum.application.maestropfe.models.ChefDeProjet;
import fr.ergosum.application.maestropfe.models.Consultant;
import fr.ergosum.application.maestropfe.models.Tache;
import fr.ergosum.application.maestropfe.repositories.ConsultantRepository;
import fr.ergosum.application.maestropfe.services.implementation.ConsultantServiceImp;
import fr.ergosum.application.maestropfe.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping("/consultant")
@CrossOrigin(origins = "*", maxAge = 3600)
public class ConsultantController {


    @Autowired
    private ConsultantServiceImp consultantServiceImp;

    @Autowired
    private Logger logger;
    @Autowired
    private StorageService storageService;
    @Autowired
    private ConsultantRepository consultantRepository;

    @PostMapping("/create")
    public Consultant createOfConsullant(Consultant consultant, @RequestParam("file") MultipartFile file) {
        String nom = storageService.store(file);
        consultant.setPhoto(nom);
        return consultantServiceImp.createConsultant(consultant);

    }

    @GetMapping("/list")
    public List<Consultant> getAllConsultant() {
        return consultantServiceImp.listConsultant();
    }

    @GetMapping("/findConsultantById/{id}")
    public Consultant getOneConsultant(@PathVariable Long id) {

        return consultantServiceImp.readOneConsultant(id);
    }

    @PutMapping("/modifier/{id}")

    public Consultant updateUser(@PathVariable Long id, Consultant chef) {


        Consultant c = consultantServiceImp.readOneConsultant(id);
        if (c != null) {
            chef.setId(id);
            if (chef.getEmail() == null) {
                String s1 = c.getEmail();
                chef.setEmail(s1);
            }
            if (chef.getUsername() == null) {
                String s1 = c.getUsername();
                chef.setUsername(s1);
            }
            if (chef.getPassword() == null) {
                String s1 = c.getPassword();
                chef.setPassword(s1);
            }
            return consultantServiceImp.modifierConsultant(chef);

        } else {
            logger.log(null, "echec de modification");
        }


        return chef;


    }

    @DeleteMapping("/deleteconsultantById/{id}")


    public HashMap<String, String> supprimerConsultant(@PathVariable long id) {
        HashMap message = new HashMap();
        try {
            Optional<Consultant> u = consultantRepository.findById(id);
            if (u.isPresent()) {
                consultantServiceImp.supprimerConsultant(id);
                message.put("etat", "deleted");
            } else {
                message.put("etat", "id was deleted");
            }
        } catch (Exception e) {
            message.put("etat", "error");
        }

        return message;

    }

}
