package fr.ergosum.application.maestropfe.controllers;



import fr.ergosum.application.maestropfe.models.Collaborateur;

import fr.ergosum.application.maestropfe.models.Consultant;
import fr.ergosum.application.maestropfe.services.implementation.CollaborateurImp;
import fr.ergosum.application.maestropfe.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/collaborateur")
public class CollaborateurController {


    @Autowired
    private CollaborateurImp collaborateurImp;

    @Autowired
    private Logger logger;
    @Autowired
    private StorageService storageService;


    @PostMapping("/create")
    public Collaborateur createOfCollaborateur(Collaborateur collaborateur, @RequestParam("file") MultipartFile file) {
        String nom=storageService.store(file);
        collaborateur.setPhoto(nom);
        return collaborateurImp.createCollaborateur(collaborateur);

    }

    @GetMapping("/list")
    public List<Collaborateur> getCollaborateur() {
        return collaborateurImp.listCollaborateur();
    }

    @GetMapping("/findCollaborateurById/{id}")
    public Collaborateur getOneCollaborateur(@PathVariable Long id) {

        return collaborateurImp.readOneCollaborateur(id);
    }

    @PutMapping("/modifier/{id}")

    public Collaborateur updateUser(@PathVariable Long id, Collaborateur chef) {


        Collaborateur c = collaborateurImp.readOneCollaborateur(id);
        if (c != null) {

            if (chef.getEmail() == null) {
                String s1 = c.getEmail();
                chef.setEmail(s1);
            }
            if (chef.getUsername() == null) {
                String s1 = c.getUsername();
                chef.setUsername(s1);
            }
            if (chef.getPassword() == null) {
                String s1 = c.getPassword();
                chef.setPassword(s1);
            }
            if (chef.getPhone() == null) {
                String s1 = c.getPhone();
                chef.setPhone(s1);
            }
            if (chef.getAdresse() == null) {
                String s1 = c.getAdresse();
                chef.setAdresse(s1);
            }

        } else {
            logger.log(null, "echec de modification");
        }
        chef.setId(id);
         collaborateurImp.modifierCollaborateur(chef);
        return chef;


    }

    @DeleteMapping("/supprimer/{id}")
    public void deleteOneChef(@PathVariable Long id) {


        try {
            Optional<Collaborateur> us = Optional.ofNullable(collaborateurImp.readOneCollaborateur(id));
            if (us.isPresent()) {
                collaborateurImp.supprimerCollaborateur(id);

            }


        } catch (Exception e) {
            logger.log(null, "erreur");
        }

    }

}
