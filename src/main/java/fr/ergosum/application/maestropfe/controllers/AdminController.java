package fr.ergosum.application.maestropfe.controllers;


import fr.ergosum.application.maestropfe.models.Admin;


import fr.ergosum.application.maestropfe.services.implementation.AdminServiceImp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("admin")
public class AdminController {


    @Autowired
    private AdminServiceImp adminServiceImp;




    @PostMapping("/create")
    public Admin createOfAdmin(Admin  admin) {

        return adminServiceImp.createAdmin(admin);

    }

    @GetMapping("/list")
    public List<Admin> getAdmin() {
        return adminServiceImp.listAdmin();
    }

    @GetMapping("findAdminById/{id}")
    public Admin getOneAdmin(@PathVariable Long id) {

        return adminServiceImp.readOne(id);
    }


}
