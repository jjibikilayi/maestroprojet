package fr.ergosum.application.maestropfe.controllers;



import fr.ergosum.application.maestropfe.models.ChefDeProjet;
import fr.ergosum.application.maestropfe.models.Consultant;
import fr.ergosum.application.maestropfe.models.Projet;
import fr.ergosum.application.maestropfe.models.User;
import fr.ergosum.application.maestropfe.services.implementation.ChefServiceImp;
import fr.ergosum.application.maestropfe.services.implementation.ConsultantServiceImp;
import fr.ergosum.application.maestropfe.services.implementation.ProjetServiceImp;
import fr.ergosum.application.maestropfe.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/projet")
public class ProjetController {



    @Autowired
    private ProjetServiceImp projetServiceImp;
    @Autowired
    private ChefServiceImp chefServiceImp;
    @Autowired
    private ConsultantServiceImp consultantServiceImp;

    @Autowired
    private Logger logger;

@Autowired
private StorageService storageService;
    @PostMapping("/createPhoto")
    public Projet createOfProjePhotot(Projet projet, @RequestParam("file") MultipartFile file) {
  String nom=storageService.store(file);
  projet.setFile(nom);
  System.out.println("ok");
        return projetServiceImp.createProjet(projet);
    }

    //création d'un projet par un chef de projet donc id de chef projet doit exister dans url
    //de même pour consultant doit être affecter lors de création de projet
    @PostMapping("/create/{idchefProjet}/{idConsulant}")
    public Projet createOfProjet(@RequestBody Projet projet,@PathVariable Long idchefProjet,@PathVariable Long idConsulant) {
 //vérifier existence de chef de projet par id je vais l'affecter au projet
        ChefDeProjet chef = chefServiceImp.readOneChef(idchefProjet);
        if(chef != null){
            projet.setChefProjetID(chef);
        }
        //vérifier existence de consultant par id je vais l'affecter au projet
        Consultant consultant = consultantServiceImp.readOneConsultant(idConsulant);
        if(consultant != null){
            System.out.println("consulant  "+consultant.getUsername());
            projet.setConsultantID(consultant);
        }
        /*String nom=storageService.store(file);
        projet.setFile(nom);*/
        System.out.println("ok");
        return projetServiceImp.createProjet(projet);
    }
    @GetMapping("/list")
    public List<Projet> getAllProjet() {
System.out.println("ok");
        return projetServiceImp.listProjet();
    }

    @GetMapping("/findProjetById/{id}")
    public Projet getOneProjet(@PathVariable Long id) {

        return projetServiceImp.readOneProjet(id);
    }


    @PutMapping("/modifier/{id}")

    public Projet updateUser(@PathVariable Long id, Projet projet ,@RequestParam Long idchef , @RequestParam Long idconsult,@RequestParam("file") MultipartFile file

                           ) {


        Projet p = projetServiceImp.readOneProjet(id);
        if (p != null) {
            projet.setId(id);
          /*  if (projet.getNom() == null) {
                String s1 = p.getNom();
                projet.setNom(s1);
            }
            if (projet.getDescription() == null) {
                String s1 = p.getDescription();
                projet.setDescription(s1);
            }
            if (projet.getDateDebut() == null) {
                Date s1 = p.getDateDebut();
                projet.setDateDebut(s1);
            }
            if (projet.getDateFin() == null) {
                Date s1 = p.getDateFin();
                projet.setDateFin(s1);
            }
         /*   if(file != null)
            {
                String str= storageService.store(file);
                projet.setFile(str);
            }
            else{

                projet.setFile(p.getFile());
            }
*/
            ChefDeProjet c = chefServiceImp.readOneChef(idchef);
            projet.setChefProjetID(c);

            Consultant con =consultantServiceImp.readOneConsultant(idconsult);
            projet.setConsultantID(con);

            String s= storageService.store(file);
            projet.setFile(s);
            if (projet.getConsultantID() == null) {
                Consultant s1 = p.getConsultantID();
                projet.setConsultantID(s1);
            }
            if (projet.getChefProjetID() == null) {
                ChefDeProjet s1 = p.getChefProjetID();
                projet.setChefProjetID(s1);
            }


        } else {
            System.out.println("echec de modification");
        }

        projet.setId(id);
       projetServiceImp.modifierProjet(projet);
       return projet;

    }

    @DeleteMapping("/supprimer/{id}")
    public void deleteOneUser(@PathVariable Long id) {


        try {
            Optional<Projet> us = Optional.ofNullable(projetServiceImp.readOneProjet(id));
            if (us.isPresent()) {
                projetServiceImp.supprimerProjet(id);

            }


        } catch (Exception e) {
            System.out.println("erreur");
        }

    }

}
