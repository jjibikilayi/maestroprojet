package fr.ergosum.application.maestropfe.controllers;



import fr.ergosum.application.maestropfe.models.*;
import fr.ergosum.application.maestropfe.repositories.TacheRepository;
import fr.ergosum.application.maestropfe.services.implementation.ChefServiceImp;
import fr.ergosum.application.maestropfe.services.implementation.CollaborateurImp;
import fr.ergosum.application.maestropfe.services.implementation.ProjetServiceImp;
import fr.ergosum.application.maestropfe.services.implementation.TacheServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/tache")
public class TacheController {


    @Autowired
    private TacheServiceImp tacheServiceImp;
    @Autowired
    private ProjetServiceImp projetServiceImp;
    @Autowired
    private TacheRepository tacheRepository;
    @Autowired
    private ChefServiceImp chefServiceImp;
    @Autowired
    private CollaborateurImp collaborateurImp;

    @Autowired
    private Logger logger;

    //création d'une tache par un chef de projet donc idchef projet doit exister dans url de creation
    //de m^me chaque tcahe , elle appartinet à un projet donc idprojet doit etre affecter à chaque tache
    //je doit verifier avec le role si role = chef de projet
    @PostMapping("/saveTache/{idprojet}/{idcollaborateur}")
    public Tache saveTache(@RequestBody Tache t, @PathVariable Long idprojet, @PathVariable Long idcollaborateur) {
        Projet p = projetServiceImp.readOneProjet(idprojet);
        System.out.println("le nome de projet est:" + p.getNom());
        t.setProjet(p);

        Collaborateur c =collaborateurImp.readOneCollaborateur(idcollaborateur);
        System.out.println("le nom de collaborateuur est:"+ c.getUsername());
        t.setCollaborateurId(c);

        return tacheServiceImp.createTache(t);
    }




    @GetMapping("/list")
    public List<Tache> getTache() {
        return tacheServiceImp.listTache();
    }

    @GetMapping("/findTacheById/{id}")
    public Tache getOneTache(@PathVariable Long id) {

        return tacheServiceImp.readOne(id);
    }

    @DeleteMapping("/supprimerTacheProjet/{id}")


    public HashMap<String, String> supprimerTacheProjet(@PathVariable long id) {
        HashMap message = new HashMap();
        try {
            Optional<Tache> u = tacheRepository.findById(id);
            if (u.isPresent()) {
                tacheServiceImp.supprimerTache(id);
                message.put("etat", "deleted");
            } else {
                message.put("etat", "id was deleted");
            }
        } catch (Exception e) {
            message.put("etat", "error");
        }

        return message;

    }
    @PutMapping("/modifier/{id}")

    public Tache updateTache(@PathVariable Long id, Tache tache , @RequestParam Long idprojet ,@RequestParam Long idcollaborateur) {

        tache.setId(id);

        Tache t = tacheServiceImp.readOne(id);
        if (t != null) {

            if (tache.getNom() == null) {
                String s1 = t.getNom();
                tache.setNom(s1);
            }
            if (tache.getDescription() == null) {
                String s1 = t.getDescription();
                tache.setDescription(s1);
            }

            Projet p =projetServiceImp.readOneProjet(idprojet);
            tache.setProjet(p);
            Collaborateur c = collaborateurImp.readOneCollaborateur(idcollaborateur);
            tache.setCollaborateurId(c);

            if (tache.getEtat() == null) {
                Etat s1 = t.getEtat();
                tache.setEtat(s1);
            }
            if (tache.getDateDebut() == null) {
                Date s1 = t.getDateDebut();
                tache.setDateDebut(s1);
            }
            if (tache.getDateFin() == null) {
                Date s1 = t.getDateFin();
                tache.setDateFin(s1);
            }

        } else {
          System.out.println("echec");
        }

        tacheServiceImp.modifierTache(tache);
        return tache;


    }
}
