package fr.ergosum.application.maestropfe.controllers;
import java.util.logging.Logger;

import fr.ergosum.application.maestropfe.models.User;
import fr.ergosum.application.maestropfe.services.implementation.UserServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/users")
public class UserController {


    @Autowired
    private UserServiceImp userServiceImp;

    @Autowired
    private Logger logger;

    @PostMapping("/create")
    public User createOfUser(User us) {

        return userServiceImp.createUser(us);

    }

    @GetMapping("/list")
    public List<User> getusers() {
        return userServiceImp.allUsers();
    }

    @GetMapping("findUserById/{id}")
    public User getOneUser(@PathVariable Long id) {

        return userServiceImp.findUserById(id);
    }


    @DeleteMapping("/deleteUserById/{id}")
    public void deleteOneUser(@PathVariable Long id) {


        try {
            Optional<User> us = Optional.ofNullable(userServiceImp.findUserById(id));
            if (us.isPresent()) {
                userServiceImp.deleteUser(id);

            }


        } catch (Exception e) {
            logger.log(null, "erreur");
        }

    }


    @PutMapping("/modifier/{id}")

    public User updateUser(@PathVariable Long id, User user) {


        User us = userServiceImp.findUserById(id);
        if (us != null) {
            user.setId(id);
            if (user.getEmail() == null) {
                String s1 = us.getEmail();
                user.setEmail(s1);
            }
            if (user.getUsername() == null) {
                String s1 = us.getUsername();
                user.setUsername(s1);
            }
            if (user.getPassword() == null) {
                String s1 = us.getPassword();
                user.setPassword(s1);
            }
            return userServiceImp.updateUser(user);

        } else {
            logger.log(null, "echec de modification");
        }


            return user;


    }
}