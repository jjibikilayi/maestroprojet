package fr.ergosum.application.maestropfe.controllers;


import fr.ergosum.application.maestropfe.models.ChefDeProjet;

import fr.ergosum.application.maestropfe.models.User;
import fr.ergosum.application.maestropfe.services.implementation.ChefServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/chefdeprojet")
public class ChefDeProjetController {




    @Autowired
    private ChefServiceImp chefServiceImp;

    @Autowired
    private Logger logger;


    @PostMapping("/create")
    public ChefDeProjet createOfChefDeProjet(ChefDeProjet chef) {
        return chefServiceImp.createChefdeProjet(chef);


    }

    @GetMapping("/list")
    public List<ChefDeProjet> getChefdeprojet() {
        return chefServiceImp.listChefDeProjet();
    }

    @GetMapping("/findChefDeProjetById/{id}")
    public ChefDeProjet getOneChefDeProjet(@PathVariable Long id) {

        return chefServiceImp.readOneChef(id);
    }



    @PutMapping("/modifier/{id}")

    public ChefDeProjet updateUser(@PathVariable Long id, ChefDeProjet chef) {


        ChefDeProjet c = chefServiceImp.readOneChef(id);
        if (c != null) {
            chef.setId(id);
            if (chef.getEmail() == null) {
                String s1 = c.getEmail();
                chef.setEmail(s1);
            }
            if (chef.getUsername() == null) {
                String s1 = c.getUsername();
                chef.setUsername(s1);
            }
            if (chef.getPassword() == null) {
                String s1 = c.getPassword();
                chef.setPassword(s1);
            }
            return chefServiceImp.modifierChefDeProjet(chef);

        } else {
            logger.log(null, "echec de modification");
        }


        return chef;


    }

    @DeleteMapping("/deletechefById/{id}")
    public void deleteOneChef(@PathVariable Long id) {


        try {
            Optional<ChefDeProjet> us = Optional.ofNullable(chefServiceImp.readOneChef(id));
            if (us.isPresent()) {
                chefServiceImp.supprimerChefDeProjet(id);

            }


        } catch (Exception e) {
            logger.log(null, "erreur");
        }

    }

}
