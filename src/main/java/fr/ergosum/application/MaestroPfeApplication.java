package fr.ergosum.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.logging.Logger;

@SpringBootApplication
@SpringBootConfiguration
public class MaestroPfeApplication {
@Bean
public Logger logger(){
    return Logger.getLogger("mylogger");
}
    public static void main(String[] args) {
        SpringApplication.run(MaestroPfeApplication.class, args);
    }

}
