FROM openjdk:17

VOLUME /tmp

COPY target/*.jar MaestroPfe-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","/MaestroPfe-0.0.1-SNAPSHOT.jar"]